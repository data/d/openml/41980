# OpenML dataset: SAT11-HAND-runtime-regression

https://www.openml.org/d/41980

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

source: http://www.cs.ubc.ca/labs/beta/Projects/SATzilla/
authors: L. Xu, F. Hutter, H. Hoos, K. Leyton-Brown
translator in coseal format: M. Lindauer with the help of Alexandre Frechette
the data do not distinguish between timeout, memout or crashes!
the status file will only have ok or timeout!
If features are "?", the instance was solved during feature computation.

Although there is no necessary alignment and dependencies between the feature processing steps,
the steps were executed in a fixed alignment.
Therefore, all feature steps depend on the previous executed ones.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41980) of an [OpenML dataset](https://www.openml.org/d/41980). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41980/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41980/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41980/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

